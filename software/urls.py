"""ScientificResearch URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from sft import views
from django.contrib.auth.views import logout_then_login
from django.conf.urls import include

urlpatterns = [


    path('admin/', admin.site.urls),

    path('index/',views.index,name="index_view"),
   
    path('signup/', views.signup, name="signup_view"),




    path('software/', views.CreateSo.as_view(), name="software_view"),

    path('directorio/', views.CreateDi.as_view(), name="directorio_view"),

    path('departamentos/', views.CreateDe.as_view(), name="departamentos_view"),





    path('listDi/', views.ListDi.as_view(), name="listDi_view"),

    path('listDe/', views.ListDe.as_view(), name="listDe_view"),

    path('listSo/', views.ListSo.as_view(), name="listSo_view"),

    path('cuentas/', include('django.contrib.auth.urls')),

  


    path('deleteDi/<int:pk>/', views.DeleteDi.as_view(), name="deleteDi_view"),


    path('deleteDe/<int:pk>/', views.DeleteDe.as_view(), name="deleteDe_view"),


    path('deleteSo/<int:pk>/', views.DeleteSo.as_view(), name="deleteSo_view"),
]

