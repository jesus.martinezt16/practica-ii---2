from django.contrib import admin

# Register your models here.
from .models import Software

admin.site.register(Software)

from .models import Departamentos

admin.site.register(Departamentos)

from .models import Directorio

admin.site.register(Directorio)