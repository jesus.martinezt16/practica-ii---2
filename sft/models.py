from django.db import models
from django.conf import settings

# Create your models here.

class Software(models.Model):

	
	nombre=models.CharField(max_length=20)
	funcion=models.CharField(max_length=140)
	tag=models.SlugField()



	def __str__(self):
		return self.nombre


class Departamentos(models.Model):

	
	departamento = models.CharField(max_length=20)
	tag=models.SlugField()



	def __str__(self):
		return self.departamento



class Directorio(models.Model):

	id = models.AutoField(primary_key=True)

	Software = models.OneToOneField(
        Software,
        on_delete=models.CASCADE,
        primary_key=False,
    )

	departamento = models.ForeignKey(Departamentos, on_delete=models.CASCADE)
	tag=models.SlugField()



	def __str__(self):
		return self.tag