from django.shortcuts import render , redirect

# Create your views here.
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login

from django.views import generic
from django.urls import reverse_lazy

from .models import Software,Departamentos,Directorio
from django.db.models import Q
#
from .forms import LoginForm


class CreateSo(generic.CreateView):
	template_name = "software.html"
	model = Software
	fields = [
		
		"nombre",
		"funcion",
		"tag",
	]
	success_url = reverse_lazy("index_view")

class CreateDe(generic.CreateView):
	template_name = "departamentos.html"
	model = Departamentos
	fields = [
		
		"departamento",
		"tag",
	]
	success_url = reverse_lazy("index_view")


class CreateDi(generic.CreateView):
	template_name = "directorio.html"
	model = Directorio
	fields = [
		
		"Software",
		"departamento",
		"tag",
	]
	success_url = reverse_lazy("index_view")










def index (request): 
	
	return render (request,"index.html",{})


#view para registro
def signup(request):

    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')
            login(request, user)
            return redirect("index_view")

        else:
            for msg in form.error_messages:
                print(form.error_messages[msg])

            return render(request = request,
                          template_name = "signup.html",
                          context={"form":form})

    form = UserCreationForm
    return render(request = request,
                  template_name = "signup.html",
                  context={"form":form})




class ListDi(generic.ListView):
	template_name = "listDi.html"
	model = Directorio

	def get_queryset(self, *args, **kwargs):
			qs = Directorio.objects.all()
			print(self.request)
			query = self.request.GET.get("q", None)
			if query is not None:
				qs = qs.filter( Q(departamento__departamento__icontains=query) | Q(Software__funcion__icontains=query) )
			return qs

	def get_context_data(self, *args, **kwargs):
		context = super(ListDi, self).get_context_data(*args, **kwargs)# context = get_context_data()
		context["message"] = "Message"
		#context["create_url"] = reverse_lazy("tweets:Create_Tweet_view")
		return context

class ListDe(generic.ListView):
	template_name = "listDe.html"
	model = Departamentos

class ListSo(generic.ListView):
	template_name = "listSo.html"
	model = Software

	

class DeleteDi(generic.DeleteView):
	template_name = "deleteDi.html"
	model = Directorio
	success_url = reverse_lazy("listDi_view")


class DeleteDe(generic.DeleteView):
	template_name = "deleteDe.html"
	model = Departamentos
	success_url = reverse_lazy("listDi_view")


class DeleteSo(generic.DeleteView):
	template_name = "deleteSo.html"
	model = Software
	success_url = reverse_lazy("listDi_view")

