
from django.contrib.auth.forms import UserCreationForm
from django import forms


class LoginForm(forms.Form):
	username = forms.CharField(max_length=24, widget=forms.TextInput())
	password = forms.CharField(max_length=24, widget=forms.PasswordInput())


